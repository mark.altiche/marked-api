# Compilation Stage
FROM python:3.11-slim-bookworm
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app/

# Install PostgreSQL development libraries
RUN apt-get update && apt-get install -y libpq-dev

# Create and activate venv
RUN python -m venv venv

# Install dependencies
RUN pip install --upgrade pip
COPY ./src/requirements.txt /app/requirements.txt
RUN pip install -Ur requirements.txt

# Copy the rest of the application code
COPY . /app/

# Start the Flask development server with automatic reloading
CMD ["flask", "run", "--host", "0.0.0.0", "--port", "8000", "--debug"]
