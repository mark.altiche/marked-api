from services import AuthService
from utilities import HttpCode, JsonResponse, validate_input, handles_exception
from validators import AuthValidator

class AuthController:

    @classmethod
    @handles_exception
    @validate_input(AuthValidator)
    def login(cls) -> JsonResponse:
        """
        Authenticate a user and generate an access token.
        """
        access_token = AuthService.authenticate()
        if access_token:
            return JsonResponse.success(data=access_token, status_code=HttpCode.SUCCESS)

        return JsonResponse.error(
            message='These credentials do not match our records.', 
            status_code=HttpCode.UNAUTHORIZED
        )

    @classmethod
    @handles_exception
    def information(cls) -> JsonResponse:
        """
        Retrieve authenticated user information.
        """
        auth_info = AuthService.information()
        return JsonResponse.success(data=auth_info, status_code=HttpCode.SUCCESS)
    
    @classmethod
    @handles_exception
    def logout(cls) -> JsonResponse:
        """
        Revoke the authentication token for the current user.
        """
        auth_info = AuthService.revokeToken()
        return JsonResponse.success(data=auth_info, status_code=HttpCode.SUCCESS)
