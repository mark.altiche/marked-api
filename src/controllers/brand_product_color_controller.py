from flask import request
from validators import BrandProductColorValidator
from services import BrandProductColorService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class BrandProductColorController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all brand product colors.
        """
        brand_product_color_products = BrandProductColorService.get_all()
        return JsonResponse.success(data=brand_product_color_products, status_code=HttpCode.SUCCESS)

    @classmethod
    @handles_exception
    @validate_input(BrandProductColorValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new brand product color.
        """
        created_brand_product_color = BrandProductColorService.create(request.json)
        return JsonResponse.success(
            data=created_brand_product_color, 
            message='Successfully created a brand product color!', 
            status_code=HttpCode.CREATED
        )

    @classmethod
    @handles_exception
    @validate_input(BrandProductColorValidator)
    def update(cls, brand_product_color_id: int) -> JsonResponse:
        """
        Update an existing brand product color.
        """
        update_brand_product_color = BrandProductColorService.update_by_id(brand_product_color_id, request.json)
        return JsonResponse.success(
            data=update_brand_product_color,
            message='Successfully updated a brand product color!', 
            status_code=HttpCode.SUCCESS
        )
    
    @classmethod
    @handles_exception
    def delete(cls, brand_product_color_id: int) -> JsonResponse:
        """
        Delete a brand product color.
        """
        BrandProductColorService.delete_by_id(brand_product_color_id)
        return JsonResponse.success(
            message='Successfully deleted a brand product color!', 
            status_code=HttpCode.SUCCESS
        )
