from flask import request
from validators import CategoryClassTypeKindValidator
from services import CategoryClassTypeKindService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class CategoryClassTypeKindController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all category class type kinds.
        """
        category_class_types = CategoryClassTypeKindService.get_all()
        return JsonResponse.success(
            data=category_class_types, 
            status_code=HttpCode.SUCCESS
        )

    @classmethod
    @handles_exception
    @validate_input(CategoryClassTypeKindValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new category class type kind.
        """
        created_category_class = CategoryClassTypeKindService.save(request.json)
        return JsonResponse.success(
            data=created_category_class, 
            message='Successfully created a category class type kind!', 
            status_code=HttpCode.CREATED
        )

    @classmethod
    @handles_exception
    @validate_input(CategoryClassTypeKindValidator)
    def update(cls, category_class_type_id: int) -> JsonResponse:
        """
        Update an existing category class type kind.
        """
        updated_category_class = CategoryClassTypeKindService.update(category_class_type_id, request.json)
        return JsonResponse.success(
            data=updated_category_class, 
            message='Successfully updated a category class type kind!', 
            status_code=HttpCode.SUCCESS
        )
    
    @classmethod
    @handles_exception
    def delete(cls, category_class_type_id: int) -> JsonResponse:
        """
        Delete a category class type kind.
        """
        CategoryClassTypeKindService.delete(category_class_type_id)
        return JsonResponse.success(
            message='Successfully deleted a category class type kind!', 
            status_code=HttpCode.SUCCESS
        )
