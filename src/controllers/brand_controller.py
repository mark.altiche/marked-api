from flask import request
from validators import BrandValidator
from services import BrandService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class BrandController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all brands.
        """
        brands = BrandService.get_all_brands()
        return JsonResponse.success(data=brands, status_code=HttpCode.SUCCESS)

    @classmethod
    @handles_exception
    @validate_input(BrandValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new brand.
        """
        created_brand = BrandService.save_brand(request.json)
        return JsonResponse.success(data=created_brand, message='Successfully created a brand!', status_code=HttpCode.CREATED)

    @classmethod
    @handles_exception
    @validate_input(BrandValidator)
    def update(cls, brand_id: int) -> JsonResponse:
        """
        Update an existing brand.
        """
        updated_brand = BrandService.update_brand(brand_id, request.json)
        return JsonResponse.success(data=updated_brand, message='Successfully updated a brand!', status_code=HttpCode.SUCCESS)
    
    @classmethod
    @handles_exception
    def delete(cls, brand_id: int) -> JsonResponse:
        """
        Delete a brand.
        """
        BrandService.delete_brand(brand_id)
        return JsonResponse.success(message='Successfully deleted a brand!', status_code=HttpCode.SUCCESS)
