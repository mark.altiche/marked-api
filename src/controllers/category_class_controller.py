from flask import request
from validators import CategoryClassValidator
from services import CategoryClassService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class CategoryClassController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all category classes.
        """
        category_classes = CategoryClassService.get_all()
        return JsonResponse.success(
            data=category_classes, 
            status_code=HttpCode.SUCCESS
        )

    @classmethod
    @handles_exception
    @validate_input(CategoryClassValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new category class.
        """
        created_category_class = CategoryClassService.save(request.json)
        return JsonResponse.success(
            data=created_category_class, 
            message='Successfully created a category class!', 
            status_code=HttpCode.CREATED
        )

    @classmethod
    @handles_exception
    @validate_input(CategoryClassValidator)
    def update(cls, category_class_id: int) -> JsonResponse:
        """
        Update an existing category class.
        """
        updated_category_class = CategoryClassService.update(category_class_id, request.json)
        return JsonResponse.success(
            data=updated_category_class, 
            message='Successfully updated a category class!', 
            status_code=HttpCode.SUCCESS
        )
    
    @classmethod
    @handles_exception
    def delete(cls, category_class_id: int) -> JsonResponse:
        """
        Delete a category class.
        """
        CategoryClassService.delete(category_class_id)
        return JsonResponse.success(
            message='Successfully deleted a category class!', 
            status_code=HttpCode.SUCCESS
        )
