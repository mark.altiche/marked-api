from flask import jsonify, request
from validators import CategoryClassTypeValidator
from services import CategoryClassTypeService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class CategoryClassTypeController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all category class types.
        """
        category_class_types = CategoryClassTypeService.get_all()
        return JsonResponse.success(
            data=category_class_types, 
            status_code=HttpCode.SUCCESS
        )

    @classmethod
    @handles_exception
    @validate_input(CategoryClassTypeValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new category class type.
        """
        created_category_class = CategoryClassTypeService.save(request.json)
        return JsonResponse.success(
            data=created_category_class, 
            message='Successfully created a category class type!', 
            status_code=HttpCode.CREATED
        )

    @classmethod
    @handles_exception
    @validate_input(CategoryClassTypeValidator)
    def update(cls, category_class_type_id: int) -> JsonResponse:
        """
        Update an existing category class type.
        """
        updated_category_class = CategoryClassTypeService.update(category_class_type_id, request.json)
        return JsonResponse.success(
            data=updated_category_class, 
            message='Successfully updated a category class type!', 
            status_code=HttpCode.SUCCESS
        )
    
    @classmethod
    @handles_exception
    def delete(cls, category_class_type_id: int) -> JsonResponse:
        """
        Delete a category class type.
        """
        CategoryClassTypeService.delete(category_class_type_id)
        return JsonResponse.success(
            message='Successfully deleted a category class type!', 
            status_code=HttpCode.SUCCESS
        )
