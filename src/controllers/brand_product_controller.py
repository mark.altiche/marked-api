from flask import request
from validators import BrandProductValidator
from services import BrandProductService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class BrandProductController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all brand products.
        """
        brand_products = BrandProductService.get_all()
        return JsonResponse.success(data=brand_products, status_code=HttpCode.SUCCESS)

    @classmethod
    @handles_exception
    @validate_input(BrandProductValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new brand product.
        """
        created_brand = BrandProductService.create(request.json)
        return JsonResponse.success(
            data=created_brand, 
            message='Successfully created a brand product!', 
            status_code=HttpCode.CREATED
        )

    @classmethod
    @handles_exception
    @validate_input(BrandProductValidator)
    def update(cls, brand_product_id: int) -> JsonResponse:
        """
        Update an existing brand product.
        """
        updated_brand = BrandProductService.update_by_id(brand_product_id, request.json)
        return JsonResponse.success(
            data=updated_brand, 
            message='Successfully updated a brand product!', 
            status_code=HttpCode.SUCCESS
        )
    
    @classmethod
    @handles_exception
    def delete(cls, brand_product_id: int) -> JsonResponse:
        """
        Delete a brand product.
        """
        BrandProductService.delete_by_id(brand_product_id)
        return JsonResponse.success(
            message='Successfully deleted a brand product!', 
            status_code=HttpCode.SUCCESS
        )
