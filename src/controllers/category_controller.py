from flask import request
from validators import CategoryValidator
from services import CategoryService
from utilities import JsonResponse, HttpCode, validate_input, handles_exception

class CategoryController:

    @classmethod
    @handles_exception
    def index(cls) -> JsonResponse:
        """
        Retrieve all categories.
        """
        categories = CategoryService.get_all_categories()
        return JsonResponse.success(data=categories, status_code=HttpCode.SUCCESS)

    @classmethod
    @handles_exception
    @validate_input(CategoryValidator)
    def store(cls) -> JsonResponse:
        """
        Create a new category.
        """
        created_category = CategoryService.save_category(request.json)
        return JsonResponse.success(data=created_category, message='Successfully created!', status_code=HttpCode.CREATED)

    @classmethod
    @handles_exception
    @validate_input(CategoryValidator)
    def update(cls, category_id: int) -> JsonResponse:
        """
        Update an existing category.
        """
        updated_category = CategoryService.update_category(category_id, request.json)
        return JsonResponse.success(data=updated_category, message='Successfully updated!', status_code=HttpCode.SUCCESS)
    
    @classmethod
    @handles_exception
    def delete(cls, category_id: int) -> JsonResponse:
        """
        Delete a category.
        """
        CategoryService.delete_category(category_id)
        return JsonResponse.success(message='Successfully deleted!', status_code=HttpCode.SUCCESS)
