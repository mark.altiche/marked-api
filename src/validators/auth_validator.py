from marshmallow import Schema, fields, validate, ValidationError

def validate_not_empty(data):
    if not data.strip():
        raise ValidationError("Field is required")

class AuthValidator(Schema):
    username = fields.Str(required=True, validate=[validate_not_empty])
    password = fields.Str(required=True, validate=[validate_not_empty])
