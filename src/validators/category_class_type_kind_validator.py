from flask import request
from marshmallow import Schema, fields, validate, validates, ValidationError
from models import CategoryClassTypeKind, CategoryClassType

class CategoryClassTypeKindValidator(Schema):
    name = fields.Str(required=True, validate=validate.Length(min=1, max=50))
    category_class_type_id = fields.Int(required=True)

    @validates('name')
    def validate_name_exists(self, value):
        existing_category = CategoryClassTypeKind.query.filter(
            (CategoryClassTypeKind.name == value) &
            (CategoryClassTypeKind.category_class_type_id == request.json['category_class_type_id'])
        ).first()

        if existing_category:
            raise ValidationError('A category class type with this name already exists.')

    @validates('category_class_type_id')
    def validate_category_class_type_id_exists(self, value):
        if CategoryClassType.query.get(value) is None:
            raise ValidationError(f'A category class with id {value} does not exist.')
