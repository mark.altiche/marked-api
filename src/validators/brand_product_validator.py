from flask import request
from marshmallow import Schema, fields, validate, validates, ValidationError
from models import Brand, BrandProduct, CategoryClassTypeKind

class BrandProductValidator(Schema):
    brand_id = fields.Int(required=True)
    category_class_type_kind_id = fields.Int(required=True)
    name = fields.Str(required=True, validate=validate.Length(min=3, max=50))
    description = fields.Str(required=True, validate=validate.Length(min=10))
    price = fields.Decimal(required=True)
        
    @validates('brand_id')
    def validate_brand_exists(self, brand_id: int):
        if Brand.query.get(brand_id) is None:
            raise ValidationError('A brand with this id does not exist.')
    
    @validates('category_class_type_kind_id')
    def validate_category_class_type_kind_exists(self, category_class_type_kind_id: int):
        if CategoryClassTypeKind.query.get(category_class_type_kind_id) is None:
            raise ValidationError('A category class type kind with this id does not exist.')
        
    @validates('name')
    def validate_name_exists(self, name: str):
        existing_brand_product = BrandProduct.query.filter(
            (BrandProduct.name == name) &
            (BrandProduct.brand_id == request.json['brand_id']) &
            (BrandProduct.category_class_type_kind_id == request.json['category_class_type_kind_id'])
        ).first()

        if existing_brand_product:
            raise ValidationError('A brand product with this name already exists.')
