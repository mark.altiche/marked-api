from flask import request
from marshmallow import Schema, fields, validate, validates, ValidationError
from models import BrandProduct, BrandProductColor

class BrandProductColorValidator(Schema):
    brand_product_id = fields.Int(required=True)
    name = fields.Str(required=True, validate=validate.Length(min=3, max=50))
        
    @validates('brand_product_id')
    def validate_brand_exists(self, brand_product_id: int):
        if BrandProduct.query.get(brand_product_id) is None:
            raise ValidationError('A brand product with this id does not exist.')
    
    @validates('name')
    def validate_name_exists(self, name: str):
        existing_brand_product_color = BrandProductColor.query.filter(
            (BrandProductColor.name == name) &
            (BrandProductColor.brand_product_id == request.json['brand_product_id'])
        ).first()

        if existing_brand_product_color:
            raise ValidationError('A brand product color with this name already exists.')
