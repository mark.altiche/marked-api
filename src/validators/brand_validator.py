from marshmallow import Schema, fields, validate, validates, ValidationError
from models import Brand

class BrandValidator(Schema):
    name = fields.Str(required=True, validate=validate.Length(min=1, max=50))

    @validates('name')
    def validate_name_exists(self, value):
        if Brand.query.filter_by(name=value).first():
            raise ValidationError('A brand with this name already exists.')
