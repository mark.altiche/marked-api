from flask import request
from marshmallow import Schema, fields, validate, validates, ValidationError
from models import Category, CategoryClass

class CategoryClassValidator(Schema):
    name = fields.Str(required=True, validate=validate.Length(min=1, max=50))
    category_id = fields.Int(required=True)

    @validates('name')
    def validate_name_exists(self, value):
        existing_category = CategoryClass.query.filter(
            (CategoryClass.name == value) &
            (CategoryClass.category_id == request.json['category_id'])
        ).first()

        if existing_category:
            raise ValidationError('A category class with this name already exists.')

    @validates('category_id')
    def validate_category_id_exists(self, value):
        if Category.query.get(value) is None:
            raise ValidationError(f'A category class with id {value} does not exist.')
