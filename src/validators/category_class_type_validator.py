from flask import request
from marshmallow import Schema, fields, validate, validates, ValidationError
from models import CategoryClassType, CategoryClass

class CategoryClassTypeValidator(Schema):
    name = fields.Str(required=True, validate=validate.Length(min=1, max=50))
    category_class_id = fields.Int(required=True)

    @validates('name')
    def validate_name_exists(self, value):
        existing_category = CategoryClassType.query.filter(
            (CategoryClassType.name == value) &
            (CategoryClassType.category_class_id == request.json['category_class_id'])
        ).first()

        if existing_category:
            raise ValidationError('A category class type with this name already exists.')

    @validates('category_class_id')
    def validate_category_class_id_exists(self, value):
        if CategoryClass.query.get(value) is None:
            raise ValidationError(f'A category class with id {value} does not exist.')
