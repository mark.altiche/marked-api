from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import AuthController
from utilities import requires_json
from routes import auth_bp

@auth_bp.route('/auth/login', methods=['POST'], endpoint='auth_login')
@requires_json
def login():
    return AuthController.login()

@auth_bp.route('/auth/information', methods=['GET'], endpoint='auth_information')
@requires_json
@jwt_required()
def information():
    return AuthController.information()

@auth_bp.route('/auth/logout', methods=['POST'], endpoint='auth_logout')
@requires_json
@jwt_required()
def logout():
    return AuthController.logout()
