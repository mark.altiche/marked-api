from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import BrandProductController
from models import BrandProduct
from utilities import check_id_exists, requires_json
from . import brand_product_bp

@brand_product_bp.route('/brand-products', methods=['GET'], endpoint='brand_products_index')
@jwt_required()
@requires_json
def index():
    return BrandProductController.index()

@brand_product_bp.route('/brand-products', methods=['POST'], endpoint='brand_products_store')
@jwt_required()
@requires_json
def store():
    return BrandProductController.store()

@brand_product_bp.route('/brand-products/<int:brand_product_id>', methods=['PATCH'], endpoint='brand_products_update')
@check_id_exists(BrandProduct)
@requires_json
@jwt_required()
def update(brand_product_id):
    return BrandProductController.update(brand_product_id)

@brand_product_bp.route('/brand-products/<int:brand_product_id>', methods=['DELETE'], endpoint='brand_products_delete')
@check_id_exists(BrandProduct)
@requires_json
@jwt_required()
def delete(brand_product_id):
    return BrandProductController.delete(brand_product_id)
