from flask import Blueprint

auth_bp = Blueprint('auth', __name__)
user_bp = Blueprint('user', __name__)
brand_bp = Blueprint('brand', __name__)
brand_product_bp = Blueprint('brand-product', __name__)
brand_product_color_bp = Blueprint('brand-product-color', __name__)
category_bp = Blueprint('category', __name__)
category_class_bp = Blueprint('category-class', __name__)
category_class_type_bp = Blueprint('category-class-type', __name__)
category_class_type_kind_bp = Blueprint('category-class-type-kind', __name__)
