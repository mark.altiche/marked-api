from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import CategoryClassTypeController
from models import CategoryClassType
from utilities import check_id_exists, requires_json
from routes import category_class_type_bp

@category_class_type_bp.route('/category-class-types', methods=['GET'], endpoint='category_class_type_index')
@jwt_required()
@requires_json
def index():
    return CategoryClassTypeController.index()

@category_class_type_bp.route('/category-class-types', methods=['POST'], endpoint='category_class_type_store')
@jwt_required()
@requires_json
def store():
    return CategoryClassTypeController.store()

@category_class_type_bp.route('/category-class-types/<int:category_class_type_id>', methods=['PATCH'], endpoint='category_class_type_update')
@check_id_exists(CategoryClassType)
@requires_json
@jwt_required()
def update(category_class_type_id):
    return CategoryClassTypeController.update(category_class_type_id)

@category_class_type_bp.route('/category-class-types/<int:category_class_type_id>', methods=['DELETE'], endpoint='category_class_type_delete')
@check_id_exists(CategoryClassType)
@requires_json
@jwt_required()
def delete(category_class_type_id):
    return CategoryClassTypeController.delete(category_class_type_id)
