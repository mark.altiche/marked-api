from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import CategoryController
from models import Category
from utilities import check_id_exists, requires_json
from . import category_bp

@category_bp.route('/categories', methods=['GET'], endpoint='categories_index')
@jwt_required()
@requires_json
def index():
    return CategoryController.index()

@category_bp.route('/categories', methods=['POST'], endpoint='categories_store')
@jwt_required()
@requires_json
def store():
    return CategoryController.store()

@category_bp.route('/categories/<int:category_id>', methods=['PATCH'], endpoint='categories_update')
@check_id_exists(Category)
@requires_json
@jwt_required()
def update(category_id):
    return CategoryController.update(category_id)

@category_bp.route('/categories/<int:category_id>', methods=['DELETE'], endpoint='categories_delete')
@check_id_exists(Category)
@requires_json
@jwt_required()
def delete(category_id):
    return CategoryController.delete(category_id)
