from flask import Blueprint
from controllers import UserController
from routes import user_bp

@user_bp.route('/users', methods=['GET'])
def index():
    return UserController.index()
