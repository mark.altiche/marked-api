from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import CategoryClassController
from models import CategoryClass
from utilities import check_id_exists, requires_json
from routes import category_class_bp

@category_class_bp.route('/category-classes', methods=['GET'], endpoint='category_class_index')
@jwt_required()
@requires_json
def index():
    return CategoryClassController.index()

@category_class_bp.route('/category-classes', methods=['POST'], endpoint='category_class_store')
@jwt_required()
@requires_json
def store():
    return CategoryClassController.store()

@category_class_bp.route('/category-classes/<int:category_class_id>', methods=['PATCH'], endpoint='category_class_update')
@check_id_exists(CategoryClass)
@requires_json
@jwt_required()
def update(category_class_id):
    return CategoryClassController.update(category_class_id)

@category_class_bp.route('/category-classes/<int:category_class_id>', methods=['DELETE'], endpoint='category_class_delete')
@check_id_exists(CategoryClass)
@requires_json
@jwt_required()
def delete(category_class_id):
    return CategoryClassController.delete(category_class_id)
