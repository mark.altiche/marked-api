from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import CategoryClassTypeKindController
from models import CategoryClassTypeKind
from utilities import check_id_exists, requires_json
from . import category_class_type_kind_bp

@category_class_type_kind_bp.route('/category-class-type-kinds', methods=['GET'], endpoint='category_class_type_kind_index')
@jwt_required()
@requires_json
def index():
    return CategoryClassTypeKindController.index()

@category_class_type_kind_bp.route('/category-class-type-kinds', methods=['POST'], endpoint='category_class_type_kind_store')
@jwt_required()
@requires_json
def store():
    return CategoryClassTypeKindController.store()

@category_class_type_kind_bp.route('/category-class-type-kinds/<int:category_class_type_kind_id>', methods=['PATCH'], endpoint='category_class_type_kind_update')
@check_id_exists(CategoryClassTypeKind)
@requires_json
@jwt_required()
def update(category_class_type_kind_id):
    return CategoryClassTypeKindController.update(category_class_type_kind_id)

@category_class_type_kind_bp.route('/category-class-type-kinds/<int:category_class_type_kind_id>', methods=['DELETE'], endpoint='category_class_type_kind_delete')
@check_id_exists(CategoryClassTypeKind)
@requires_json
@jwt_required()
def delete(category_class_type_kind_id):
    return CategoryClassTypeKindController.delete(category_class_type_kind_id)
