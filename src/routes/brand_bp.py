from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import BrandController
from models import Brand
from utilities import check_id_exists, requires_json
from . import brand_bp

@brand_bp.route('/brands', methods=['GET'], endpoint='brands_index')
@jwt_required()
@requires_json
def index():
    return BrandController.index()

@brand_bp.route('/brands', methods=['POST'], endpoint='brands_store')
@jwt_required()
@requires_json
def store():
    return BrandController.store()

@brand_bp.route('/brands/<int:brand_id>', methods=['PATCH'], endpoint='brands_update')
@check_id_exists(Brand)
@requires_json
@jwt_required()
def update(brand_id):
    return BrandController.update(brand_id)

@brand_bp.route('/brands/<int:brand_id>', methods=['DELETE'], endpoint='brands_delete')
@check_id_exists(Brand)
@requires_json
@jwt_required()
def delete(brand_id):
    return BrandController.delete(brand_id)
