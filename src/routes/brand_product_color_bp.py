from flask import Blueprint
from flask_jwt_extended import jwt_required
from controllers import BrandProductColorController
from models import BrandProductColor
from utilities import check_id_exists, requires_json
from . import brand_product_color_bp

@brand_product_color_bp.route('/brand-product-colors', methods=['GET'], endpoint='brand_product_colors_index')
@jwt_required()
@requires_json
def index():
    return BrandProductColorController.index()

@brand_product_color_bp.route('/brand-product-colors', methods=['POST'], endpoint='brand_product_colors_store')
@jwt_required()
@requires_json
def store():
    return BrandProductColorController.store()

@brand_product_color_bp.route('/brand-product-colors/<int:brand_product_color_id>', methods=['PATCH'], endpoint='brand_product_colors_update')
@check_id_exists(BrandProductColor)
@requires_json
@jwt_required()
def update(brand_product_color_id):
    return BrandProductColorController.update(brand_product_color_id)

@brand_product_color_bp.route('/brand-product-colors/<int:brand_product_color_id>', methods=['DELETE'], endpoint='brand_product_colors_delete')
@check_id_exists(BrandProductColor)
@requires_json
@jwt_required()
def delete(brand_product_color_id):
    return BrandProductColorController.delete(brand_product_color_id)
