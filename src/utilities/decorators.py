import re
from functools import wraps
from flask import request
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound
from app import db
from utilities import JsonResponse, HttpCode

def validate_input(validator_cls):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                validator = validator_cls()
                errors = validator.validate(request.json)

                if errors:
                    return JsonResponse.error(
                        errors=errors,
                        message='The given data was invalid.',
                        status_code=HttpCode.MISDIRECTED
                    )

                return func(*args, **kwargs)

            except Exception as e:
                return JsonResponse.error(message=str(e), status_code=HttpCode.SERVER_ERROR)

        return wrapper

    return decorator

def check_id_exists(model_cls):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                snake_case_name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', model_cls.__name__).lower()
                model_id = kwargs.get(f"{snake_case_name}_id")

                if not model_cls.query.get(model_id):
                    error_message = f"{model_cls.__name__} with ID {model_id} not found"
                    return JsonResponse.error(message=error_message, status_code=HttpCode.NOT_FOUND)

                return func(*args, **kwargs)

            except NoResultFound as e:
                return JsonResponse.error(message=str(e), status_code=HttpCode.NOT_FOUND)

        return wrapper

    return decorator

def requires_json(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if request.content_type != 'application/json':
            return JsonResponse.error(message='Invalid Content-Type. Use application/json', status_code=HttpCode.UNSUPPORTED)
        return func(*args, **kwargs)

    return wrapper

def handles_exception(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return result
        except Exception as e:
            db.session.rollback()
            return JsonResponse.error(message=str(e), status_code=HttpCode.SERVER_ERROR)
        finally:
            db.session.close()

    return wrapper
