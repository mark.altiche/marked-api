from flask import jsonify

class JsonResponse:
    @classmethod
    def success(cls, data=None, message=None, success=True, status_code=200):
        response = {
            'status': status_code,
            'success': success,
            'data': data
        }

        if message:
            response['message'] = message

        return jsonify(response), status_code
    
    @classmethod
    def error(cls, errors=None, message='Internal Server Error', success=False, status_code=500):
        response = {
            'status': status_code,
            'success': success,
            'message': message,
        }

        if errors:
            response['errors'] = errors

        return jsonify(response), status_code
