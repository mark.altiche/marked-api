from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, TIMESTAMP, Numeric
from . import db
from datetime import datetime

class BrandProductColor(db.Model):
    __tablename__ = 'brand_product_colors'

    id = Column(Integer, primary_key=True, autoincrement=True)
    brand_product_id = Column(Integer, ForeignKey('brand_products.id'), index=True)
    name = Column(String(80), nullable=False)

    __table_args__ = (
        UniqueConstraint('brand_product_id', 'name', name='_brand_product_name_uc'),
    )

    created_at = Column(TIMESTAMP, nullable=True, default=datetime.utcnow)
    updated_at = Column(TIMESTAMP, nullable=True, onupdate=datetime.utcnow)
    deleted_at = Column(TIMESTAMP, nullable=True)

    def as_dict(self):
        return {
            'id': self.id,
            'brand_product_id': self.brand_product_id,
            'name': self.name,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'deleted_at': self.deleted_at
        }
