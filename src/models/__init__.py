from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

db = SQLAlchemy()
jwt = JWTManager()

from .user import User
from .brand import Brand
from .brand_product import BrandProduct
from .brand_product_color import BrandProductColor
from .category import Category
from .category_class import CategoryClass
from .category_class_type import CategoryClassType
from .category_class_type_kind import CategoryClassTypeKind
from .token_blacklist import TokenBlocklist
