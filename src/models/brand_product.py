from sqlalchemy import Column, Integer, String, Text, ForeignKey, UniqueConstraint, TIMESTAMP, Numeric
from sqlalchemy.orm import relationship
from . import db
from datetime import datetime

class BrandProduct(db.Model):
    __tablename__ = 'brand_products'

    id = Column(Integer, primary_key=True, autoincrement=True)
    brand_id = Column(Integer, ForeignKey('brands.id'), index=True)
    category_class_type_kind_id = Column(Integer, ForeignKey('category_class_type_kinds.id'), index=True)
    name = Column(String(80), nullable=False)
    description = Column(Text, nullable=False)
    price = Column(Numeric(precision=10, scale=2), nullable=False)

    __table_args__ = (
        UniqueConstraint('brand_id', 'category_class_type_kind_id', 'name', name='_brand_category_name_uc'),
    )

    created_at = Column(TIMESTAMP, nullable=True, default=datetime.utcnow)
    updated_at = Column(TIMESTAMP, nullable=True, onupdate=datetime.utcnow)
    deleted_at = Column(TIMESTAMP, nullable=True)

    def as_dict(self):
        return {
            'id': self.id,
            'brand_id': self.brand_id,
            'category_class_type_kind_id': self.category_class_type_kind_id,
            'name': self.name,
            'description': self.description,
            'price': str(self.price),
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'deleted_at': self.deleted_at
        }
