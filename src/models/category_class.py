from sqlalchemy import ForeignKey, UniqueConstraint
from . import db
from datetime import datetime

class CategoryClass(db.Model):
    __tablename__ = 'category_classes'
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    category_id = db.Column(db.Integer, ForeignKey('categories.id'), index=True)
    name = db.Column(db.String(80), nullable=False)

    __table_args__ = (
        UniqueConstraint('category_id', 'name', name='_category_id_name_uc'),
    )

    created_at = db.Column(db.TIMESTAMP, nullable=True, default=datetime.utcnow)
    updated_at = db.Column(db.TIMESTAMP, nullable=True, onupdate=datetime.utcnow)
    deleted_at = db.Column(db.TIMESTAMP, nullable=True)

    def as_dict(self):
        return {
            'id': self.id,
            'category_id': self.category_id,
            'name': self.name,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'deleted_at': self.deleted_at
        }
