from models import CategoryClassTypeKind
from typing import List, Dict

class CategoryClassTypeKindService:

    @classmethod
    def get_all(cls) -> List[Dict]:
        """
        Get a list of all category class type kinds.

        Returns:
            List[Dict]
        """
        return [category_class_type_kind.as_dict() for category_class_type_kind in CategoryClassTypeKind.query.all()]
    
    @classmethod
    def get_by_id(cls, category_class_type_kind_id: int) -> CategoryClassTypeKind:
        """
        Get a category class type kind by its ID.
        """
        category_class_type_kind = CategoryClassTypeKind.query.get(category_class_type_kind_id)
        return category_class_type_kind

    @classmethod
    def save(cls, data: Dict) -> Dict:
        """
        Save a new category class type kind to the database.
        """
        new_category_class_type_kind = CategoryClassTypeKind(
            category_class_type_id=data['category_class_type_id'], 
            name=data['name']
        )

        CategoryClassTypeKind.query.session.add(new_category_class_type_kind)
        CategoryClassTypeKind.query.session.commit()

        return new_category_class_type_kind.as_dict()

    @classmethod
    def update(cls, category_class_type_kind_id: int, data: Dict) -> Dict:
        """
        Update an existing category class type kind.
        """
        category_class_type_kind = cls.get_by_id(category_class_type_kind_id)

        category_class_type_kind.category_class_type_id = data['category_class_type_id']
        category_class_type_kind.name = data['name']

        CategoryClassTypeKind.query.session.add(category_class_type_kind)
        CategoryClassTypeKind.query.session.commit()

        return category_class_type_kind.as_dict()

    @classmethod
    def delete(cls, category_class_type_kind_id: int) -> None:
        """
        Delete a category class type kind by its ID.
        """
        category = cls.get_by_id(category_class_type_kind_id)

        CategoryClassTypeKind.query.session.delete(category)
        CategoryClassTypeKind.query.session.commit()
