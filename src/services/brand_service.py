from models import Brand
from typing import List, Dict

class BrandService:

    @classmethod
    def get_all_brands(cls) -> List[Dict]:
        """
        Get a list of all brands.
        
        Returns:
            List[Dict]: A list of dictionaries representing brands.
        """
        return [brand.as_dict() for brand in Brand.query.all()]
    
    @classmethod
    def get_brand_by_id(cls, brand_id: int) -> Brand:
        """
        Get a brand by its ID.
        """
        brand = Brand.query.get(brand_id)
        return brand

    @classmethod
    def save_brand(cls, data: Dict) -> Dict:
        """
        Save a new brand to the database.
        """
        new_brand = Brand(name=data['name'])

        Brand.query.session.add(new_brand)
        Brand.query.session.commit()

        return new_brand.as_dict()

    @classmethod
    def update_brand(cls, brand_id: int, data: Dict) -> Dict:
        """
        Update an existing brand.
        """
        brand = cls.get_brand_by_id(brand_id)
        brand.name = data['name']

        Brand.query.session.add(brand)
        Brand.query.session.commit()

        return brand.as_dict()

    @classmethod
    def delete_brand(cls, brand_id: int) -> None:
        """
        Delete a brand by its ID.
        """
        brand = cls.get_brand_by_id(brand_id)

        Brand.query.session.delete(brand)
        Brand.query.session.commit()
