from models import BrandProduct
from typing import List, Dict

class BrandProductService:

    @classmethod
    def get_all(cls) -> List[Dict]:
        """
        Get a list of all brand products.
        
        Returns:
            List[Dict]: A list of dictionaries representing brand products.
        """
        return [brand_product.as_dict() for brand_product in BrandProduct.query.all()]
    
    @classmethod
    def find_by_id(cls, brand_product_id: int) -> BrandProduct:
        """
        Get a brand product by its ID.
        """
        brand_product = BrandProduct.query.get(brand_product_id)
        return brand_product

    @classmethod
    def create(cls, data: Dict) -> Dict:
        """
        Save a new brand product.
        """
        new_brand_product = BrandProduct(
            brand_id=data['brand_id'],
            category_class_type_kind_id=data['category_class_type_kind_id'],
            name=data['name'],
            description=data['description'],
            price=data['price']
        )

        BrandProduct.query.session.add(new_brand_product)
        BrandProduct.query.session.commit()

        return new_brand_product.as_dict()

    @classmethod
    def update_by_id(cls, brand_product_id: int, data: Dict) -> Dict:
        """
        Update an existing brand product.
        """
        brand_product = cls.find_by_id(brand_product_id)
        brand_product.category_class_type_kind_id = data['category_class_type_kind_id']
        brand_product.name = data['name']
        brand_product.description = data['description']
        brand_product.price = data['price']

        BrandProduct.query.session.add(brand_product)
        BrandProduct.query.session.commit()

        return brand_product.as_dict()

    @classmethod
    def delete_by_id(cls, brand_product_id: int) -> None:
        """
        Delete a brand product by its ID.
        """
        brand_product = cls.find_by_id(brand_product_id)

        BrandProduct.query.session.delete(brand_product)
        BrandProduct.query.session.commit()
