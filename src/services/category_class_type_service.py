from models import CategoryClassType
from typing import List, Dict

class CategoryClassTypeService:

    @classmethod
    def get_all(cls) -> List[Dict]:
        """
        Get a list of all category class types.

        Returns:
            List[Dict]
        """
        return [category_class_type.as_dict() for category_class_type in CategoryClassType.query.all()]
    
    @classmethod
    def get_by_id(cls, category_class_type_id: int) -> CategoryClassType:
        """
        Get a category class type by its ID.
        """
        category_class_type = CategoryClassType.query.get(category_class_type_id)
        return category_class_type

    @classmethod
    def save(cls, data: Dict) -> Dict:
        """
        Save a new category class type to the database.
        """
        new_category_class_type = CategoryClassType(
            category_class_id=data['category_class_id'], 
            name=data['name']
        )

        CategoryClassType.query.session.add(new_category_class_type)
        CategoryClassType.query.session.commit()

        return new_category_class_type.as_dict()

    @classmethod
    def update(cls, category_class_type_id: int, data: Dict) -> Dict:
        """
        Update an existing category class type.
        """
        category_class_type = cls.get_by_id(category_class_type_id)

        category_class_type.category_class_id = data['category_class_id']
        category_class_type.name = data['name']

        CategoryClassType.query.session.add(category_class_type)
        CategoryClassType.query.session.commit()

        return category_class_type.as_dict()

    @classmethod
    def delete(cls, category_class_type_id: int) -> None:
        """
        Delete a category class type by its ID.
        """
        category = cls.get_by_id(category_class_type_id)

        CategoryClassType.query.session.delete(category)
        CategoryClassType.query.session.commit()
