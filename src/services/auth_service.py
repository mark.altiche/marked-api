import datetime
from flask import request
from flask_jwt_extended import create_access_token, get_jwt, get_jwt_identity, verify_jwt_in_request
from models import TokenBlocklist

class AuthService:

    @classmethod
    def authenticate(cls) -> str:
        """
        Authenticate a user and generate an access token.
        """
        data = request.get_json()
        username = data.get('username', None)
        password = data.get('password', None)
        
        if cls._is_invalid_credentials(username, password):
            return None

        access_token = create_access_token(identity=username)
        return access_token
    
    @classmethod
    def _is_invalid_credentials(cls, username: str, password: str) -> bool:
        """
        Check if the provided credentials are invalid.
        """
        return username != 'admin' or password != 'admin'
    
    @classmethod
    def information(cls) -> any:
        """
        Retrieve information about the authenticated user.
        """
        current_user = get_jwt_identity()
        return current_user

    @classmethod
    def revokeToken(cls) -> bool:
        """
        Revoke the current authentication token.
        """
        jti = get_jwt()["jti"]
        TokenBlocklist.query.session.add(TokenBlocklist(jti=jti))
        TokenBlocklist.query.session.commit()
        return True
