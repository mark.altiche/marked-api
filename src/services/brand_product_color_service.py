from models import BrandProductColor
from typing import List, Dict

class BrandProductColorService:

    @classmethod
    def get_all(cls) -> List[Dict]:
        """
        Get a list of all brand product colors.
        
        Returns:
            List[Dict]: A list of dictionaries representing brand product colors.
        """
        return [brand_product_color.as_dict() for brand_product_color in BrandProductColor.query.all()]
    
    @classmethod
    def find_by_id(cls, brand_product_color_id: int) -> BrandProductColor:
        """
        Get a brand product color by its ID.
        """
        brand_product_color = BrandProductColor.query.get(brand_product_color_id)
        return brand_product_color

    @classmethod
    def create(cls, data: Dict) -> Dict:
        """
        Save a new brand product color.
        """
        new_brand_product_color = BrandProductColor(
            brand_product_id=data['brand_product_id'],
            name=data['name'],
        )

        BrandProductColor.query.session.add(new_brand_product_color)
        BrandProductColor.query.session.commit()

        return new_brand_product_color.as_dict()

    @classmethod
    def update_by_id(cls, brand_product_color_id: int, data: Dict) -> Dict:
        """
        Update an existing brand product color.
        """
        brand_product_color = cls.find_by_id(brand_product_color_id)
        brand_product_color.name = data['name']

        BrandProductColor.query.session.add(brand_product_color)
        BrandProductColor.query.session.commit()

        return brand_product_color.as_dict()

    @classmethod
    def delete_by_id(cls, brand_product_color_id: int) -> None:
        """
        Delete a brand product color by its ID.
        """
        brand_product_color = cls.find_by_id(brand_product_color_id)

        BrandProductColor.query.session.delete(brand_product_color)
        BrandProductColor.query.session.commit()
