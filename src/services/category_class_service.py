from models import CategoryClass
from typing import List, Dict

class CategoryClassService:

    @classmethod
    def get_all(cls) -> List[Dict]:
        """
        Get a list of all category classes.

        Returns:
            List[Dict]
        """
        return [category_class.as_dict() for category_class in CategoryClass.query.all()]
    
    @classmethod
    def get_by_id(cls, category_class_id: int) -> CategoryClass:
        """
        Get a category class by its ID.
        """
        category = CategoryClass.query.get(category_class_id)
        return category

    @classmethod
    def save(cls, data: Dict) -> Dict:
        """
        Save a new category class to the database.
        """
        new_category_class = CategoryClass(
            category_id=data['category_id'], 
            name=data['name']
        )

        CategoryClass.query.session.add(new_category_class)
        CategoryClass.query.session.commit()

        return new_category_class.as_dict()

    @classmethod
    def update(cls, category_class_id: int, data: Dict) -> Dict:
        """
        Update an existing category class.
        """
        category_class = cls.get_by_id(category_class_id)

        category_class.category_id = data['category_id']
        category_class.name = data['name']

        CategoryClass.query.session.add(category_class)
        CategoryClass.query.session.commit()

        return category_class.as_dict()

    @classmethod
    def delete(cls, category_class_id: int) -> None:
        """
        Delete a category class by its ID.
        """
        category = cls.get_by_id(category_class_id)

        CategoryClass.query.session.delete(category)
        CategoryClass.query.session.commit()
