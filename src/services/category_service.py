from models import Category
from typing import List, Dict

class CategoryService:

    @classmethod
    def get_all_categories(cls) -> List[Dict]:
        """
        Get a list of all categories.
        
        Returns:
            List[Dict]: A list of dictionaries representing categories.
        """
        return [category.as_dict() for category in Category.query.all()]
    
    @classmethod
    def get_category_by_id(cls, category_id: int) -> Category:
        """
        Get a category by its ID.
        """
        category = Category.query.get(category_id)
        return category

    @classmethod
    def save_category(cls, data: Dict) -> Dict:
        """
        Save a new category to the database.
        """
        new_category = Category(name=data['name'])

        Category.query.session.add(new_category)
        Category.query.session.commit()

        return new_category.as_dict()

    @classmethod
    def update_category(cls, category_id: int, data: Dict) -> Dict:
        """
        Update an existing category.
        """
        category = cls.get_category_by_id(category_id)
        category.name = data['name']

        Category.query.session.add(category)
        Category.query.session.commit()

        return category.as_dict()

    @classmethod
    def delete_category(cls, category_id: int) -> None:
        """
        Delete a category by its ID.
        """
        category = cls.get_category_by_id(category_id)

        Category.query.session.delete(category)
        Category.query.session.commit()
