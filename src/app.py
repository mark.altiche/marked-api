from config import Config
from flask import Flask
from flask_migrate import Migrate
from models import TokenBlocklist, db, jwt
from importlib import import_module

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    jwt.init_app(app)
    db.init_app(app)
    migrate = Migrate(app, db)

    blueprint_names = [
        'auth_bp', 'user_bp', 'category_bp', 'category_class_bp', 
        'category_class_type_bp', 'category_class_type_kind_bp',
        'brand_bp', 'brand_product_bp', 'brand_product_color_bp'
    ]

    for name in blueprint_names:
        module = import_module(f'routes.{name}')
        app.register_blueprint(getattr(module, name), url_prefix='/api')

    return app

@jwt.token_in_blocklist_loader
def check_if_token_revoked(jwt_header, jwt_payload: dict) -> bool:
    jti = jwt_payload["jti"]
    token = db.session.query(TokenBlocklist.id).filter_by(jti=jti).scalar()

    return token is not None

if __name__ == '__main__':
    app_instance = create_app()
    app_instance.run(debug=True)
