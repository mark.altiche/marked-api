APP_IMAGE = application
DB_IMAGE = database

up:
	docker-compose up -d

up-build:
	docker-compose up -d --build

down:
	docker-compose down

build:
	docker-compose build

build-no-cache:
	docker-compose build --no-cache

bash-app:
	docker-compose exec -it ${APP_IMAGE} /bin/bash
	
bash-db:
	docker-compose exec -it ${DB_IMAGE} /bin/bash

database:
	docker-compose exec -it database

db_init:
	docker-compose exec -it ${APP_IMAGE} flask db init

db_upgrade:
	docker-compose exec -it ${APP_IMAGE} flask db upgrade

db_downgrade:
	docker-compose exec -it ${APP_IMAGE} flask db downgrade

db_migrate:
	docker-compose exec -it ${APP_IMAGE} flask db migrate
	docker-compose exec -it ${APP_IMAGE} flask db upgrade
